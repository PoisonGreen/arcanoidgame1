package com.ads.arcanoid.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import static com.ads.arcanoid.View.Player.Action.MOVELEFT;

/**
 * Created by ga_nesterchuk on 27.01.2016.
 */
public class Player extends ImageActor {

    public static enum Action {
        MOVELEFT,
        MOVERIGHT,
        STAND
    }

    private Action action;
    private float step;

    public Player(TextureRegion img, float x, float y, float width, float height, float step) {
        super(img, x, y, width, height);
        action = Action.STAND;
        this.step = step;
    }

    public Player(Texture img, float x, float y, float step) {
        super(img, x, y);
        action = Action.STAND;
        this.step = step;
    }

    public Player(Texture img, float x, float y, float width, float height, float step) {
        super(img, x, y, width, height);
        action = Action.STAND;
        this.step = step;
    }

    public Player(TextureRegion img, float x, float y, float step) {
        super(img, x, y);
        action = Action.STAND;
        this.step = step;
    }

    public void act(float delta) {
        switch (action) {
            case MOVELEFT:
                moveBy(-step*delta, 0);
                if (getX() < 0)
                    setX(0);
                break;
            case MOVERIGHT:
                moveBy(step*delta, 0);
                if (getX() + getWidth() > 920)
                    setX(920 - getWidth());
                break;
        }
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
