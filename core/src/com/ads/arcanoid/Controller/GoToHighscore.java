package com.ads.arcanoid.Controller;

import com.ads.arcanoid.ArcanoidGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Гриша on 27.01.2016.
 */
public class GoToHighscore extends ClickListener {
    public void clicked(InputEvent event, float x, float y) {
        ArcanoidGame.getInstance().moveToHighscore();
    }
}